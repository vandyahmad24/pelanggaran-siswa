<?php

use App\Http\Controllers\Admin\LaporanController;
use App\Http\Controllers\Admin\PelanggaranController;
use App\Http\Controllers\Admin\SiswaController as AdminSiswaController;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\SanksiController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\SiswaController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return redirect('/login');
});


Auth::routes();
Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/lupa-katasandi', [HomeController::class, 'reset'])->name('reset-password');
Route::post('/lupa-katasandi', [HomeController::class, 'resetPost'])->name('reset-password.store');
Route::group(['prefix' => 'admin', 'middleware' => ['cekadmin','auth'] ], function()
{
    Route::get('/dashboard', [AdminController::class, 'index'])->name('dashboard-admin');
    Route::get('/reset-password/{id}', [AdminController::class, 'resetPassword'])->name('reset-password-admin.store');
    Route::get('/pelanggaran', [PelanggaranController::class, 'index'])->name('pelanggaran-admin');
    Route::get('/add-pelanggaran', [PelanggaranController::class, 'add'])->name('add-pelanggaran-admin');
    Route::post('/add-pelanggaran', [PelanggaranController::class, 'post'])->name('post-pelanggaran-admin');
    Route::get('/delete-pelanggaran/{id}', [PelanggaranController::class, 'delete'])->name('delete-pelanggaran-admin');
    Route::get('/edit-pelanggaran/{id}', [PelanggaranController::class, 'edit'])->name('edit-pelanggaran-admin');
    Route::post('/put-pelanggaran/{id}', [PelanggaranController::class, 'put'])->name('put-pelanggaran-admin');

    Route::resource('siswa-admin', AdminSiswaController::class);
    Route::resource('sanksi-admin', SanksiController::class);
    Route::resource('user-admin', UserController::class);

    Route::get('/tambah-pelanggaran/{id}', [AdminController::class, 'tambahPelanggaran'])->name('add-pelanggaran-siswa');
    Route::post('/tambah-pelanggaran-siswa/{id}', [AdminController::class, 'tambahPelanggaranSiswa'])->name('tambah-pelanggaran-siswa');
    Route::get('/delete-pelanggaran/{id}/{user_id}', [AdminController::class, 'deletePelanggaranSiswa'])->name('delete-pelanggaran-siswa');

    Route::get('/laporan', [LaporanController::class, 'index'])->name('laporan-admin');
});
Route::get('/cetak', [LaporanController::class, 'cetak'])->name('cetak-all-admin');
Route::get('/cetak-pelanggaran/siswa/{id}', [LaporanController::class, 'cetakPelanggaranSiswa'])->name('cetak-pelanggaran-siswa-admin');
Route::resource('profile-admin',ProfileController::class);

Route::group(['prefix' => 'siswa', 'middleware' => ['ceksiswa','auth'] ], function()
{
  Route::get('/dashboard', [SiswaController::class, 'index'])->name('dashboard-siswa');
  Route::post('/send-message', [SiswaController::class, 'sendMessage'])->name('send-message-siswa');
});

Route::group(['prefix' => 'guru', 'middleware' => ['cekguru','auth'] ], function()
{
  Route::get('/dashboard', [AdminController::class, 'index'])->name('dashboard-guru');
});