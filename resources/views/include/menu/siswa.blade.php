<li class="nav-item {{ (request()->is('siswa/dashboard*')) ? 'active' : '' }}  ">
    <a class="nav-link" href="{{route('dashboard-siswa')}}">
      <i class="material-icons">dashboard</i>
      <p>Dashboard</p>
    </a>
  </li>