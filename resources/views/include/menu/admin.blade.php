@if(Auth::user()->level=='admin')
<li class="nav-item {{ (request()->is('admin/dashboard*')) ? 'active' : '' }}  ">
    <a class="nav-link" href="{{route('dashboard-admin')}}">
        <i class="material-icons">dashboard</i>
        <p>Dashboard</p>
    </a>
</li>
@else
<li class="nav-item {{ (request()->is('guru/dashboard*')) ? 'active' : '' }}  ">
  <a class="nav-link" href="{{route('dashboard-guru')}}">
      <i class="material-icons">dashboard</i>
      <p>Dashboard</p>
  </a>
</li>

@endif
<li class="nav-item ">
    <a class="nav-link" href="{{route('pelanggaran-admin')}}">
        <i class="material-icons">close</i>
        <p>Data Pelanggaran</p>
    </a>
</li>
<li class="nav-item ">
    <a class="nav-link" href="{{route('siswa-admin.index')}}">
        <i class="material-icons">person</i>
        <p>Data Siswa</p>
    </a>
</li>
<li class="nav-item ">
    <a class="nav-link" href="{{route('sanksi-admin.index')}}">
        <i class="material-icons">priority_high</i>
        <p>Sanksi</p>
    </a>
</li>
@if(Auth::user()->level == "admin")
<li class="nav-item ">
    <a class="nav-link" href="{{route('user-admin.index')}}">
        <i class="material-icons">manage_accounts</i>
        <p>User</p>
    </a>
</li>
@endif
<li class="nav-item ">
    <a class="nav-link" href="{{route('laporan-admin')}}">
        <i class="material-icons">print</i>
        <p>Cetak Laporan</p>
    </a>
</li>
