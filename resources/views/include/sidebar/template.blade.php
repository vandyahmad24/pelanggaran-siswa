<!-- sidebar -->
<div class="sidebar" data-color="purple" data-background-color="white" data-image="{{asset('adminpage/assets/img/sidebar-1.jpg')}}">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
    <div class="logo"><a href="{{route('home')}}" class="simple-text logo-normal">
        <img src="{{asset('adminpage/logo.png')}}" width="25%" alt="SMK Sekar Bumi Nusantara">
      </a></div>
    <div class="sidebar-wrapper">
      <ul class="nav">
        @if(Auth::user()->level=='admin')
        @include('include.menu.admin')
        @elseif(Auth::user()->level=='siswa')
        @include('include.menu.siswa')
        @else
        @include('include.menu.admin')
        @endif
      </ul>
    </div>
  </div>
  <!-- endsidebar -->