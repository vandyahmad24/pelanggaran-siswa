@extends('layouts.newlogin')

@section('content')


<div class="wrap-login100 p-t-85 p-b-20">
    <form method="POST" action="{{ route('login') }}" class="login100-form validate-form">
        @csrf
        <span class="login100-form-avatar">
            <img src="{{asset('loginpage/images/logo.png')}}" alt="AVATAR">
        </span>
        <span class="login100-form-title p-b-70">
            LOGIN SISTEM PELANGGARAN SISWA SMK SEKAR BUMI NUSANTARA
        </span>
        @error('email')
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>{{ $message }}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @enderror
        @error('password')
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>{{ $message }}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @enderror
        @if ($message = Session::get('success'))
        <div class="alert alert-primary  ">
            <button type="button" class="close" data-dismiss="alert">×</button>	
            <strong>{{ $message }}</strong>
        </div>
        @endif
        <div class="wrap-input100 validate-input m-t-20 m-b-25" data-validate="Enter Email">
            <input class="input100 @error('email') is-invalid @enderror" name="email" type="text" required>
            <span class="focus-input100" data-placeholder="Email"></span>
        </div>

        <div class="wrap-input100 validate-input m-b-50" data-validate="Enter password">
            <input class="input100 @error('password') is-invalid @enderror" name="password" required type="password">
            <span class="focus-input100" data-placeholder="Password"></span>
        </div>

        <div class="container-login100-form-btn">
            <button class="login100-form-btn" type="submit">
                Masuk
            </button>
        </div>
        <ul class="login-more p-t-20">
            <li class="m-b-8">
                <span class="txt1">
                    Lupa
                </span>

                <a href="{{route('reset-password')}}" class="txt2">
                    Kata Sandi?
                </a>
            </li>
        </ul>
    </form>
</div>



@endsection
