@extends('layouts.adminpage')

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                  <div class="card-header card-header-success">
                    <h3 class="card-title">Tambah Pelanggaran</h3>
                  </div>
                  <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form method="POST" action="{{route('post-pelanggaran-admin')}}">
                        @csrf
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group ">
                            <label class="">Kode Pelanggaran</label>
                            <input type="text" name="kode_pelanggaran" class="form-control" required>
                          </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group ">
                              <label class="">Bentuk Pelanggaran</label>
                              <input type="text" name="bentuk_pelanggaran" class="form-control" required>
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group ">
                              <label class="">Point</label>
                              <input type="number" name="point" class="form-control" required>
                            </div>
                          </div>
                      
                      </div>
                    
                      <button type="submit" class="btn btn-primary pull-right">Tambah</button>
                      <div class="clearfix"></div>
                    </form>
                  </div>
                </div>
              </div>
        </div>
    </div>
</div>
@push('addon-script')

@endpush

@endsection
