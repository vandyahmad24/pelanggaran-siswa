@extends('layouts.adminpage')

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-success">
                        <h3 class="card-title">Tambah Pelanggaran Siswa {{$siswa->name}}</h3>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <form method="POST" action="{{route('tambah-pelanggaran-siswa',$siswa->id)}}">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group ">
                                        <label class="">Waktu Pelanggaran</label>
                                        <input type="date" name="waktu_pelanggaran" class="form-control" required
                                            value="<?=date('Y-m-d') ?>">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group ">
                                        <label class="">Pelanggaran</label>
                                        <select class="form-control select2" name="pelanggaran_id" id="">
                                            @foreach ($pelanggaran as $p)
                                            <option value="{{$p->id}}">{{$p->bentuk_pelanggaran}} || Point {{$p->point}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary pull-right">Tambah</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('addon-script')

@endpush

@endsection
