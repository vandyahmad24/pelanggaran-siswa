@extends('layouts.adminpage')

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header card-header-danger">
                        <h3 class="card-title">Data Pelanggaran</h3>
                    </div>
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success  ">
                        <button type="button" class="close" data-dismiss="alert">×</button>	
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif
                    @if ($message = Session::get('delete'))
                    <div class="alert alert-danger  ">
                        <button type="button" class="close" data-dismiss="alert">×</button>	
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif
                    <div class="card-body table-responsive">
                        @if (Auth::user()->level=='guru')
                        <a href="{{route('add-pelanggaran-admin')}}" class="btn btn-success">+ Tambah Pelanggaran</a>
                        @endif
                       
                        <table class="table table-hover table-bordered tableDatatables mt-3">
                            <thead class="text-warning">
                                <tr>
                                    <th>No</th>
                                    <th>Kode Pelanggaran</th>
                                    <th>Bentuk/Jenis Pelanggaran</th>
                                    <th>Point</th>
                                    @if (Auth::user()->level=='guru')
                                    <th>Aksi</th>  
                                    @endif
                                  
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($master as $m)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$m->kode_pelanggaran}}</td>
                                    <td>{{$m->bentuk_pelanggaran}}</td>
                                    <td>{{$m->point}}</td>
                                    @if (Auth::user()->level=='guru')
                                    <td>
                                        <a href="{{route('edit-pelanggaran-admin',$m->id)}}"  class="btn btn-warning">Edit</a>
                                        <a href="{{route('delete-pelanggaran-admin',$m->id)}}" class="btn btn-danger">Hapus</a>
                                    </td>  
                                    @endif
                                    
                                </tr>
                                @endforeach
                                
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('addon-script')

@endpush

@endsection
