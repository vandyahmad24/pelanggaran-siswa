<!DOCTYPE html>
<html>
<head>
	<title>Laporan</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h2>Laporan Pelanggaran Semua Siswa</h2>
		<h3>SMK Sekar Bumi Nusantara</h3>
	</center>

	<table class='table table-bordered'>
		<thead>
			<tr>
				<th>No</th>
                <th>NIS</th>
                <th>Nama</th>
                <th>Kelas</th>
                <th>Jurusan</th>
                <th>Jumlah Point</th>
                <th>Sanksi</th>
			</tr>
		</thead>
		<tbody>
            @foreach ($user as $u)
                                
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$u->nis}}</td>
                <td>{{$u->name}}</td>
                <td>{{$u->profile->kelas}}</td>
                <td>{{$u->profile->jurusan}}</td>
                <td>{{$u->profile->jumlah_point}}</td>
                <td>
                    {{Bantuan::getSanksi($u->profile->jumlah_point)}}

                </td>
            </tr>
			@endforeach
		</tbody>
	</table>

</body>
</html>