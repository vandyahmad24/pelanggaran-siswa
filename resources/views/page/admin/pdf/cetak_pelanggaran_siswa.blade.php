<!DOCTYPE html>
<html>
<head>
	<title>Laporan</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h2>Laporan Pelanggaran Siswa {{$user->name}}</h2>
		<h3>SMK Sekar Bumi Nusantara</h3>
	</center>

	<table class='table table-bordered'>
		<thead>
			<tr>
				<th>No</th>
                <th>Waktu Pelanggaran</th>
                <th>Kode Pelanggaran</th>
                <th>Bentuk Pelanggaran</th>
                <th>Point Pelanggaran</th>
			</tr>
		</thead>
		<tbody>
            @foreach ($pelanggaran as $p)         
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$p->waktu_pelanggaran}}</td>
                <td>{{$p->MasterPelanggaran->kode_pelanggaran}}</td>
                <td>{{$p->MasterPelanggaran->bentuk_pelanggaran}}</td>
                <td>{{$p->MasterPelanggaran->point}}</td>
            </tr>
			@endforeach
		</tbody>
        <tfoot>
            <tr>
                <td colspan="4">Total Point</td>
                <td>{{$user->profile->jumlah_point}}</td>
            </tr>
			<tr>
                <td colspan="2">Sanksi</td>
                <td colspan="3" style="text-align:right">{{Bantuan::getSanksi($user->profile->jumlah_point)}}</td>
            </tr>
			
        </tfoot>
	</table>

</body>
</html>