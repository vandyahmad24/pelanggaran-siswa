@extends('layouts.adminpage')

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h3 class="card-title">Data User</h3>
                    </div>
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success  ">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif
                    @if ($message = Session::get('delete'))
                    <div class="alert alert-danger  ">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif
                    <div class="card-body table-responsive">
                        <a href="{{route('user-admin.create')}}" class="btn btn-success">+ User</a>
                        <table class="table table-hover table-bordered tableDatatables mt-3">
                            <thead class="text-warning">
                                <tr>
                                    <th>No</th>
                                    <th>No ID / NIK</th>
                                    <th>Email</th>
                                    <th>Nama User</th>
                                    <th>Level</th>
                                    <th>No HP</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $u)
                                
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$u->nis}}</td>
                                    <td>{{$u->email}}</td>
                                    <td>{{$u->name}}</td>
                                    <td>{{$u->level}}</td>
                                    <td>{{$u->no_hp}}</td>
                                    <td>
                                        <div class="d-flex flex-row">
                                                @if(Auth::user()->id != $u->id)
                                                <a href="{{route('user-admin.edit',$u->id)}}"
                                                    class="btn btn-warning">Edit</a>
                                                <form action="{{route('user-admin.destroy',$u->id)}}" method="post">
                                                    @method('DELETE')
                                                    @csrf
                                                    <button type="submit" class="btn btn-danger">
                                                        Hapus
                                                    </button>
                                                </form>
                                                @else
                                                <a href="{{route('user-admin.edit',$u->id)}}"
                                                    class="btn btn-warning">Edit</a>
                                                @endif
                                        </div>
                                    </td>
                                </tr>
                                @endforeach


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('addon-script')

@endpush

@endsection
