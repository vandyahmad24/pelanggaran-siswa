@extends('layouts.adminpage')

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-success">
                        <h3 class="card-title">Edit User {{$user->name}}</h3>
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        @if ($message = Session::get('error'))
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                        @endif
                        @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                        @endif
                        <form method="POST" action="{{route('user-admin.update',$user->id)}}">
                            @method("PUT")
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group ">
                                        <label class="">No Identitas/NIK</label>
                                        <input type="number" name="nis" class="form-control" required value="{{$user->nis}}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group ">
                                        <label class="">Nama</label>
                                        <input type="text" name="name" class="form-control" required value="{{$user->name}}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group ">
                                        <label class="">Email</label>
                                        <input type="text" name="email" class="form-control" required value="{{$user->email}}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group ">
                                        <label for="level">Level</label>
                                        <select class="form-control" name="level" id="level">
                                            <option value="admin" {{$user->level == "admin"  ? 'selected' : ''}} >Admin</option>
                                            <option value="guru" {{$user->level == "guru"  ? 'selected' : ''}} >Guru</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group ">
                                        <label for="jenis_kelamin">Jenis Kelamin</label>
                                        <select class="form-control" name="jenis_kelamin" id="jenis_kelamin">
                                            <option value="laki-laki" {{$user->jenis_kelamin == "laki-laki"  ? 'selected' : ''}} >Laki - laki</option>
                                            <option value="perempuan" {{$user->jenis_kelamin == "perempuan"  ? 'selected' : ''}} >Perempuan</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group ">
                                        <label class="">No HP</label>
                                        <input type="number" name="no_hp" class="form-control" required value="{{$user->no_hp}}">
                                    </div>
                                </div>
                               
                                <button type="submit" class="btn btn-primary pull-right">Update</button>
                                <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('addon-script')

@endpush

@endsection
