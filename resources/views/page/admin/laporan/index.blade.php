@extends('layouts.adminpage')

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h3 class="card-title">Data User</h3>
                    </div>
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success  ">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif
                    @if ($message = Session::get('delete'))
                    <div class="alert alert-danger  ">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif
                    <div class="card-body table-responsive">
                        <a href="{{route('cetak-all-admin')}}" target="_blank" class="btn btn-success">Cetak Semua Pelanggaran</a>
                        <table class="table table-hover table-bordered tableDatatables mt-3">
                            <thead class="text-warning">
                                <tr>
                                    <th>No</th>
                                    <th>NIS</th>
                                    <th>Nama</th>
                                    <th>Kelas</th>
                                    <th>Jurusan</th>
                                    <th>Jumlah Point</th>
                                    <th>Sanksi</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($user as $u)
                                
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$u->nis}}</td>
                                    <td>{{$u->name}}</td>
                                    <td>{{$u->profile->kelas}}</td>
                                    <td>{{$u->profile->jurusan }}</td>
                                    <td>{{$u->profile->jumlah_point}}</td>
                                    <td>
                                        {{Bantuan::getSanksi($u->profile->jumlah_point)}}

                                    </td>
                                    <td>
                                        @if($u->profile->jumlah_point > 0)
                                       <a target="_blank" href="{{route('cetak-pelanggaran-siswa-admin',$u->id)}}" class="btn btn-info">Cetak Detail Pelanggaran</a> 
                                        @else
                                        -
                                        @endif
                                       </td>
                                </tr>
                                @endforeach


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('addon-script')

@endpush

@endsection
