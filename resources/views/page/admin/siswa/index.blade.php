@extends('layouts.adminpage')

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h3 class="card-title">Data Siswa</h3>
                    </div>
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success  ">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif
                    @if ($message = Session::get('delete'))
                    <div class="alert alert-danger  ">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif
                    <div class="card-body table-responsive">
                        <a href="{{route('siswa-admin.create')}}" class="btn btn-success">Tambah Siswa</a>
                        <table class="table table-hover table-bordered tableDatatables mt-3">
                            <thead class="text-warning">
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>NIS</th>
                                    <th>Email</th>
                                    <th>Kelas</th>
                                    <th>Jurusan</th>
                                    <th>Point</th>
                                    <th>Tempat, Tanggal Lahir</th>
                                    <th>Nama Orang Tua</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($siswa as $s)
                                
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$s->name}}</td>
                                    <td>{{$s->nis}}</td>
                                    <td>{{$s->email}}</td>
                                    <td>{{$s->profile->kelas}}</td>
                                    <td>{{$s->profile->jurusan}}</td>
                                    <td>{{$s->profile->jumlah_point}}</td>
                                    <td>{{$s->profile->tempat_lahir}} , {{$s->profile->tanggal_lahir}}</td>
                                    <td>{{$s->profile->orang_tua}}</td>
                                    <td>
                                        <div class="d-flex flex-row">
                                            <a href="{{route('siswa-admin.show',$s->id)}}" class="btn btn-info">Point Pelanggaran</a>
                                            <a href="{{route('siswa-admin.edit',$s->id)}}"
                                                class="btn btn-warning">Edit</a>
                                            <form action="{{route('siswa-admin.destroy',$s->id)}}" method="post">
                                                @method('DELETE')
                                                @csrf
                                                <button type="submit" class="btn btn-danger">
                                                    Hapus
                                                </button>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('addon-script')

@endpush

@endsection
