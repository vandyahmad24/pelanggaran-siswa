@extends('layouts.adminpage')

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h3 class="card-title">Data Pelanggaran Siswa <b>{{$siswa->name}}</b> </h3>
                    </div>
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success  ">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif
                    @if ($message = Session::get('delete'))
                    <div class="alert alert-danger  ">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif
                    <div class="card-body table-responsive">
                        <a href="{{route('add-pelanggaran-siswa',$siswa->id)}}" class="btn btn-success">+ Pelanggaran</a>
                        <table class="table table-hover table-bordered tableDatatables mt-3">
                            <thead class="text-warning">
                                <tr>
                                    <th>No</th>
                                    <th>Waktu Pelanggaran</th>
                                    <th>Pelanggaran</th>
                                    <th>Point</th>
                                    <th>Sanksi</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($pelanggaran as $p)
                                
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$p->waktu_pelanggaran}}</td>
                                    <td>{{$p->MasterPelanggaran->bentuk_pelanggaran}}</td>
                                    <td>{{$p->MasterPelanggaran->point}}</td>
                                    <td>
                                        {{Bantuan::getSanksi($p->MasterPelanggaran->point)}}
                                    </td>
                                    
                                    <td>
                                        <div class="d-flex flex-row">
                                            <a href="{{route('delete-pelanggaran-siswa',[$p->id,$siswa->id])}}"
                                                    class="btn btn-danger">Delete</a>
                                          
                                        </div>
                                    </td>
                                </tr>
                                @endforeach


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('addon-script')

@endpush

@endsection
