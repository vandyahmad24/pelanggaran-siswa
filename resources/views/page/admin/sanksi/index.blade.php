@extends('layouts.adminpage')

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h3 class="card-title">Data Sanksi</h3>
                    </div>
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success  ">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif
                    @if ($message = Session::get('delete'))
                    <div class="alert alert-danger  ">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif
                    <div class="card-body table-responsive">
                        @if (Auth::user()->level=='guru')
                        <a href="{{route('sanksi-admin.create')}}" class="btn btn-success">+ Sanksi</a>
                        @endif
                        <table class="table table-hover table-bordered tableDatatables mt-3">
                            <thead class="text-warning">
                                <tr>
                                    <th>No</th>
                                    <th>Kode Sanksi</th>
                                    <th>Sanksi</th>
                                    <th>Poin</th>
                                    @if (Auth::user()->level=='guru')
                                    <th>Aksi</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($sanksi as $s)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$s->kode_sanksi}}</td>
                                    <td>{{$s->sanksi}}</td>
                                    <td>{{$s->point_min}} - {{$s->point_max}}</td>
                                    @if (Auth::user()->level=='guru')
                                    <td>
                                        <div class="d-flex flex-row">
                                            <a href="{{route('sanksi-admin.edit',$s->id)}}"
                                                class="btn btn-warning">Edit</a>
                                            <form action="{{route('sanksi-admin.destroy',$s->id)}}" method="post">
                                                @method('DELETE')
                                                @csrf
                                                <button type="submit" class="btn btn-danger">
                                                    Hapus
                                                </button>
                                            </form>
                                        </div>
                                    </td>  
                                    @endif
                                    
                                </tr>
                                @endforeach


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('addon-script')

@endpush

@endsection
