@extends('layouts.adminpage')

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                  <div class="card-header card-header-success">
                    <h3 class="card-title">Ganti Password {{$auth->name}}</h3>
                  </div>
                  <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">×</button>	
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">×</button>	
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif
                    <form method="POST" action="{{route('profile-admin.update',$auth->id)}}">
                        @method('PUT')
                        @csrf
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group ">
                            <label class="">Password Sekarang</label>
                            <input type="password" name="current_password" class="form-control" required>
                          </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group ">
                              <label class="">Password Baru</label>
                              <input type="password" name="password" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group ">
                              <label class="">Konfirmasi Password</label>
                              <input type="password" name="password_confirmation" class="form-control" required>
                            </div>
                        </div>
                      </div>
                      <button type="submit" class="btn btn-primary pull-right">Ganti Password</button>
                      <div class="clearfix"></div>
                    </form>
                  </div>
                </div>
              </div>
        </div>
    </div>
</div>
@push('addon-script')

@endpush

@endsection
