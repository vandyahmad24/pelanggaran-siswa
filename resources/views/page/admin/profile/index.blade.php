@extends('layouts.adminpage')

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                  <div class="card-header card-header-info">
                    <h3 class="card-title">Edit Profile {{$auth->name}}</h3>
                  </div>
                  <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">×</button>	
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">×</button>	
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif
                    <form method="POST" action="{{route('profile-admin.store')}}">
                        @csrf
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group ">
                            <label class="">Nama</label>
                            <input type="text" name="name" class="form-control" value="{{$auth->name}}" required>
                          </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group ">
                              <label class="">NIK</label>
                              <input type="text" name="nis" class="form-control" required value="{{$auth->nis}}">
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group ">
                              <label class="">Email</label>
                              <input type="text" name="email" class="form-control" required value="{{$auth->email}}">
                            </div>
                          </div>
                      </div>
                      <a href="{{route('profile-admin.show',$auth->id)}}" class="btn btn-warning pull-right">Ganti Password</a>
                      <button type="submit" class="btn btn-primary pull-right">Update</button>
                      <div class="clearfix"></div>
                    </form>
                  </div>
                </div>
              </div>
        </div>
    </div>
</div>
@push('addon-script')

@endpush

@endsection
