@extends('layouts.adminpage')

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-md-12">
              <div class="card">
                <div class="card-header card-header-tabs card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <span class="nav-tabs-title"><h4>Total Pelanggaran</h4></span>
                    </div>
                  </div>
                </div>
                <div class="card-body">
                  <div class="tab-content">
                    <div class="tab-pane active" id="profile">
                      <center>
                      <h1>{{$totalpelanggaran}}</h1>
                      </center>
                    </div>
                   
                  </div>
                </div>
              </div>
              <div class="card">
                <div class="card-header card-header-tabs card-header-success">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <span class="nav-tabs-title"><h4>Total Point</h4></span>
                    </div>
                  </div>
                </div>
                <div class="card-body">
                  <div class="tab-content">
                    <div class="tab-pane active" id="profile">
                         <center>
                        <h1>{{$totalpoint}}</h1>
                        </center>
                    </div>
                   
                  </div>
                </div>
              </div>
              <div class="card">
                <div class="card-header card-header-tabs card-header-warning">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <span class="nav-tabs-title"><h4>Total Siswa</h4></span>
                    </div>
                  </div>
                </div>
                <div class="card-body">
                  <div class="tab-content">
                    <div class="tab-pane active" id="profile">
                       <center>
                        <h1>{{$totalsiswa}}</h1>
                        </center>
                    </div>
                   
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-9 col-md-12">
              <div class="row">
                <div class="card">
                  <div class="card-header card-header-danger">
                    <h3 class="card-title">Top 10 Point Pelanggaran</h3>
                  </div>
                  <div class="card-body table-responsive">
                    <table class="table table-hover tableDatatables">
                      <thead class="text-warning">
                        <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Point</th>
                      </tr></thead>
                      <tbody>
                        @foreach ($pelanggaran as $p)
                        <tr>
                          <td>{{$loop->iteration}}</td>
                          <td>{{$p->user->name}}</td>
                          <td>{{$p->jumlah_point}}</td>
                        </tr>
                        @endforeach
                        
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              @if (Auth::user()->level=='admin')
              <div class="row">
                <div class="card">
                  <div class="card-header card-header-info">
                    <h3 class="card-title">Notifikasi Terbaru</h3>
                  </div>
                  <div class="container">
                  @if ($message = Session::get('success'))
                  <div class="alert alert-success  ">
                      <button type="button" class="close" data-dismiss="alert">×</button>
                      <strong>{{ $message }}</strong>
                  </div>
                  @endif
                </div>
                  <div class="card-body table-responsive">
                    <table class="table table-hover tableDatatables">
                      <thead class="text-warning">
                        <tr>
                        <th>No</th>
                        <th>Pembuat</th>
                        <th>Pesan</th>
                        <th>Tindakan</th>
                        <th>Aksi</th>
                      </tr></thead>
                      <tbody>
                        @foreach ($notifikasi as $n)
                        <tr>
                          <td>{{$loop->iteration}}</td>
                          <td>{{$n->user->name}}</td>
                          <td>{{$n->pesan}}</td>
                          <td>{{$n->tindakan==null ? '-':$n->tindakan}}</td>
                          <td> <a href="{{route('reset-password-admin.store',$n->id)}}" class="btn btn-info" onclick="return confirm('Apakah Anda Yakin Mereset Password user ini?')">Reset Password</a></td>
                        </tr>
                        @endforeach
                        
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              @endif
           
              
            </div>
            
          </div>
    </div>
  </div>
  @endsection