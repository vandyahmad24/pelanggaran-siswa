@extends('layouts.adminpage')

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="card">
            <center>
                <h1 style="font-family:'Courier New'">Top 10 Point Pelanggaran</h1>
            </center>
            <div class="row">
                @foreach ($pelanggaran as $p)
                <div class="col-lg-3 col-md-6 col-sm-6 border m-3">
                    <div class="card-header card-header-warning card-header-icon">
                        <center>
                            <h3 class="card-title">{{$p->user->name}}</h3>
                            <h4 class="card-title">{{$p->jumlah_point}} Point</h4>

                        </center>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="card">
            <center>
                <h2 class="mb-5 mt-3">Pelanggaranku</h2>
            </center>
            <div class="row">
                <div class="col-lg-12">

                    <div class="card-header card-header-primary">
                        <h3 class="card-title">Data Siswa</h3>
                    </div>
                    <div class="card-body table-responsive">
                        <a href="{{route('cetak-pelanggaran-siswa-admin',Auth::user()->id)}}" class="btn btn-info">Cetak
                            Pelanggaranku</a>
                        <table class="table table-hover table-bordered tableDatatables mt-3">
                            <thead class="text-warning">
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Jenis Pelanggaran</th>
                                    <th>Sanksi</th>
                                    <th>Poin</th>
                                </tr>
                            </thead>
                            <tbody class="text-bold">
                                @foreach ($pelanggaranuser as $s)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$s->waktu_pelanggaran}}</td>
                                    <td>{{$s->MasterPelanggaran->bentuk_pelanggaran}}</td>
                                    <td>{{Bantuan::getSanksi($s->MasterPelanggaran->point)}}</td>
                                    <td>{{$s->MasterPelanggaran->point}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="4">Total Point</td>
                                    <td>{{Auth::user()->profile->jumlah_point}}</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header card-header-danger">
                        <h3 class="card-title">Hubungi Admin</h3>
                    </div>
                    @if ($message = Session::get('error'))
                    <div class="alert alert-warning  ">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif
                    <form action="{{route('send-message-siswa')}}" method="POST">
                        @csrf
                        <div class="card-body table-responsive">
                            <div class="col-md-12">
                                <div class="form-group ">
                                    <label class="">Nama</label>
                                    <input type="text" name="name" class="form-control" value="{{Auth::user()->name}}"
                                        readonly>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group ">
                                    <label class="">nis</label>
                                    <input type="text" name="nis" class="form-control" value="{{Auth::user()->nis}}"
                                        readonly>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group ">
                                    <label class="">Pesan</label>
                                    <input type="text" name="message" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group ">
                                    <label for="jk">Admin</label>
                                    <select class="form-control" name="admin" id="jk">
                                        @foreach ($admin as $a)
                                        <option value="{{$a->id}}">{{$a->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Kirim Pesan</button>
                            <div class="clearfix"></div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
