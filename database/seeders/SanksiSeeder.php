<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SanksiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sanksi')->insert([
            'kode_sanksi'=>'SANKSI_0001',
            'sanksi'=>'Peringatan dari guru BK',
            'point_min'=>'0',
            'point_max'=>'25'
        ]);
        DB::table('sanksi')->insert([
            'kode_sanksi'=>'SANKSI_0002',
            'sanksi'=>'Surat Peringatan Pertama (Pemanggilan Wali Murid)',
            'point_min'=>'26',
            'point_max'=>'50'
        ]);
        DB::table('sanksi')->insert([
            'kode_sanksi'=>'SANKSI_0003',
            'sanksi'=>'Surat Peringatan Kedua (Pemanggilan Wali Murid dan Skorsing)',
            'point_min'=>'51',
            'point_max'=>'75'
        ]);
        DB::table('sanksi')->insert([
            'kode_sanksi'=>'SANKSI_0004',
            'sanksi'=>'Surat Peringatan Ketiga (Pemanggilan Wali Murid dan diKeluarkan)',
            'point_min'=>'76',
            'point_max'=>'100'
        ]);
    }
}
