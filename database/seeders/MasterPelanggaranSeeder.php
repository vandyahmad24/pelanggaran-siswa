<?php

namespace Database\Seeders;

use App\Models\MasterPelanggaran;
use Illuminate\Database\Seeder;

class MasterPelanggaranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $master = new MasterPelanggaran();
        $master->kode_pelanggaran = "KD001";
        $master->bentuk_pelanggaran="Bolos Sekolah";
        $master->point="30";
        $master->save();
        
    }
}
