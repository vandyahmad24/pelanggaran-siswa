<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jk = ['laki-laki','perempuan'];
        DB::table('users')->insert([
            'name' => Str::random(10),
            'email'=>'admin@gmail.com',
            'nis' => "0001",
            'password' => Hash::make('12341234'),
            'jenis_kelamin'=> $jk[rand(0,1)],
            'level'=>'admin'
        ]);

        DB::table('users')->insert([
            'name' => Str::random(10),
            'email'=>'user@gmail.com',
            'nis' => "0002",
            'password' => Hash::make('12341234'),
            'jenis_kelamin'=> $jk[rand(0,1)],
            'level'=>'guru'
        ]);

        $siswa = new User();
        $siswa->name=Str::random(10);
        $siswa->email="siswa@gmail.com";
        $siswa->nis="0003";
        $siswa->password=Hash::make('12341234');
        $siswa->jenis_kelamin = $jk[rand(0,1)];
        $siswa->level="siswa";
        $siswa->save();
        DB::table('profiles')->insert([
            'user_id'=>$siswa->id,
            'kelas'=>'X',
            'jurusan'=>'TK Pembina',
            'jumlah_point'=>'0',
            'tempat_lahir'=>'batang',
            'tanggal_lahir'=>'1998-04-24',
            'orang_tua'=>'mustafif'
        ]);
    }
}
