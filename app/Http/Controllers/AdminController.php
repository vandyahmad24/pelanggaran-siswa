<?php

namespace App\Http\Controllers;

use App\Models\MasterPelanggaran;
use App\Models\Notifikasi;
use App\Models\PelanggaranSiswa;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
class AdminController extends Controller
{
    public function index()
    {
        $pelanggaran = Profile::orderBy('jumlah_point','desc')->get();
        $totalpelanggaran = MasterPelanggaran::count();
        $siswa = User::where('level','siswa')->get();
        $totalsiswa = count($siswa);
        $totalpoint = Profile::sum('jumlah_point');
        $notifikasi = Notifikasi::with('user')->latest()->get();
    
        return view('page.admin.dashboard', compact('pelanggaran', 'totalpelanggaran','totalsiswa','totalpoint','notifikasi'));
    }
    public function tambahPelanggaran($id)
    {
       $siswa=User::find($id);
       $pelanggaran = MasterPelanggaran::all();
       return view('page.admin.pelanggaran.tambahpelanggaran', compact('siswa','pelanggaran'));
    }
    public function tambahPelanggaranSiswa(Request $request, $id)
    {
        $data=$request->all();
        $data['user_id']=$id;
        PelanggaranSiswa::create($data);
        $sums = PelanggaranSiswa::where('user_id',$id)->get();
        $total=0;
        foreach($sums as $s){
            $master = MasterPelanggaran::find($s->pelanggaran_id);
            $total += $master->point;
        }
        $siswa = Profile::where('user_id',$id)->first();
        $siswa->jumlah_point=$total;
        $siswa->save();
        return redirect()->route('siswa-admin.show',$id)->with('success','Pelanggaran Berhasil ditambah');

    }
    public function deletePelanggaranSiswa($id, $user_id)
    {
       $rs = PelanggaranSiswa::find($id);
       $rs->delete();

       $sums = PelanggaranSiswa::where('user_id',$user_id)->get();
       $total=0;
       foreach($sums as $s){
           $master = MasterPelanggaran::find($s->pelanggaran_id);
           $total += $master->point;
       }
       $siswa = Profile::where('user_id',$user_id)->first();
       $siswa->jumlah_point=$total;
       $siswa->save();


       return redirect()->route('siswa-admin.show',$user_id)->with('success','Pelanggaran Berhasil ditambah');
    }
    public function resetPassword($id)
    {
        $notifikasi = Notifikasi::find($id);
        
       $user = User::find($notifikasi->user_id);
       $user->password = Hash::make('smk_sekar_bumi_nusantara');
       $user->save();
       $notifikasi->tindakan = "Reset Password $user->name Berhasil menjadi smk_sekar_bumi_nusantara";
        $notifikasi->save();

       return redirect()->route('dashboard-admin')->with('success',"User atas nama $user->name berhasil direset password menjadi smk_sekar_bumi_nusantara ");
    }
}
