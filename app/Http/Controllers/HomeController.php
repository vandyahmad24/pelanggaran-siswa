<?php

namespace App\Http\Controllers;

use App\Models\Notifikasi;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $role = Auth::user()->level; 
        // dd($role);
        switch ($role) {
            case 'admin':
                    return redirect('/admin/dashboard'); 
                break;
            case 'guru':
                return redirect('/guru/dashboard'); 
                break; 
            case 'siswa':
                return redirect('/siswa/dashboard'); 
                break; 
            default:
                    return '/login'; 
                break;
        }
    }
    public function reset()
    {
        return view('page.reset');
    }
    public function resetPost(Request $request)
    {
       $user = User::where('email',$request->email)->first();
       if($user==null){
        return redirect()->route('reset-password')->with('error','Email yang anda masukan tidak terdaftar');
       }else{
           $notifikasi = new Notifikasi();
           $notifikasi->user_id = $user->id;
           $notifikasi->pesan = "User $user->name meminta reset password";
           $notifikasi->save();
           return redirect()->route('login')->with('success','Permohonan Reset Password Sudah Terkirim Silahkan Hubungi Admin');
       }
    }
}
