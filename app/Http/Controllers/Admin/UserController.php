<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('level','!=','siswa')->get();
        return view('page.admin.users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('page.admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validated = $request->validate([
            'nis' => 'required|numeric|unique:users,nis',
            'name' => 'required|string',
            'email' => 'required|email|unique:users,email',
            'password'=>'required|string|confirmed',
            'level'=>'required',
            'jenis_kelamin'=>'required',
            'no_hp'=>'required|numeric|unique:users,no_hp',
        ]);
        $data = $request->all();
        $data['password'] = Hash::make($data['password']);
        User::create($data);
        return redirect()->route('user-admin.index')->with('success','User berhasil ditambah');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('page.admin.users.edit', compact('user'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'nis' => 'required|numeric',
            'email' => 'required|email',
            'name' => 'required|string',
            'level'=>'required',
            'jenis_kelamin'=>'required',
            'no_hp'=>'required|numeric',
        ]);
        $cek = User::where('no_hp',$request->no_hp)->first();
        // cek no hp
        if(isset($cek)){
            if($cek->id != $id){
                return redirect()->route('user-admin.edit',$id)->with('error','No HP Sudah Ada');
            }
        }
        // cek nis
        $cek = User::where('nis',$request->nis)->first();
        // cek no hp
        if(isset($cek)){
            if($cek->id != $id){
                return redirect()->route('user-admin.edit',$id)->with('error','Nomer Identitas sudah terdaftar');
            }
        }

        // cek emial
        $cek = User::where('email',$request->email)->first();
        // cek email
        if(isset($cek)){
            if($cek->id != $id){
                return redirect()->route('user-admin.edit',$id)->with('error','Email sudah terdaftar');
            }
        }
        
        $data = $request->all();
        $user = User::find($id);
        $user->update($data);
        return redirect()->route('user-admin.index')->with('success','User berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->route('user-admin.index')->with('success','User berhasil dihapus');
    }
}
