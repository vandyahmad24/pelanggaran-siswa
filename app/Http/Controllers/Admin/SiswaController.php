<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PelanggaranSiswa;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $siswa = User::where('level','siswa')->get();
        // dd($siswa);
        return view("page.admin.siswa.index", compact('siswa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
        return view("page.admin.siswa.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'nis' => 'required|unique:users,nis',
            'name' => 'required|string',
            'email' => 'required|email|unique:users,email',
            'kelas' => 'required|string',
            'jurusan'=>'required',
            'tempat_lahir'=>'required',
            'orang_tua'=>'required',
            'tanggal_lahir'=>'required',

        ]);
        $user = new User();
        $user->name=$request->name;
        $user->nis=$request->nis;
        $user->email=$request->email;
        $user->jenis_kelamin=$request->jenis_kelamin;
        $user->password=Hash::make('smk_bisa');
        $user->level = "siswa";
        $user->save();
        $profil = new Profile();
        $profil->user_id=$user->id;
        $profil->kelas=$request->kelas;
        $profil->jurusan=$request->jurusan;
        $profil->tanggal_lahir=$request->tanggal_lahir;
        $profil->tempat_lahir=$request->tempat_lahir;
        $profil->orang_tua=$request->orang_tua;
        $profil->save();
        return redirect()->route('siswa-admin.index')->with('success','Siswa berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {  
        $siswa = User::find($id);
        $pelanggaran = PelanggaranSiswa::where('user_id',$id)->get();
        return view('page.admin.siswa.show', compact('pelanggaran','siswa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $siswa = User::find($id);
        return view("page.admin.siswa.edit", compact('siswa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'nis' => 'required',
            'name' => 'required|string',
            'email' => 'required|email',
            'kelas' => 'required|string',
            'jurusan'=>'required',
            'tempat_lahir'=>'required',
            'orang_tua'=>'required',
            'tanggal_lahir'=>'required',
        ]);
        $cek = User::where('nis',$request->nis)->first();

        if(isset($cek)){
            if($cek->id != $id){
                return redirect()->route('siswa-admin.edit',$id)->with('error','NIS Sudah terdaftar');
            }
        }

        $cek = User::where('email',$request->email)->first();

        if(isset($cek)){
            if($cek->id != $id){
                return redirect()->route('siswa-admin.edit',$id)->with('error','Email Sudah terdaftar');
            }
        }


        $user= User::find($id);
        $user->name=$request->name;
        $user->email=$request->email;
        $user->nis=$request->nis;
        $user->jenis_kelamin=$request->jenis_kelamin;
        $user->save();

        $profil = Profile::where('user_id',$id)->first();
        $profil->kelas=$request->kelas;
        $profil->jurusan=$request->jurusan;
        $profil->tanggal_lahir=$request->tanggal_lahir;
        $profil->tempat_lahir=$request->tempat_lahir;
        $profil->orang_tua=$request->orang_tua;
        $profil->save();
        return redirect()->route('siswa-admin.index')->with('success','Siswa berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user=User::find($id);
        $profile=Profile::where('user_id',$id)->first();
        $profile->delete();
        $user->delete();
        return redirect()->route('siswa-admin.index')->with('success','Siswa berhasil dihapus');
    }
}
