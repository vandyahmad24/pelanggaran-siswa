<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Sanksi;
use Illuminate\Http\Request;

class SanksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sanksi = Sanksi::all();
        return view('page.admin.sanksi.index', compact('sanksi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('page.admin.sanksi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'kode_sanksi' => 'required|string',
            'sanksi' => 'required|string',
            'point_min'=>'required|numeric',
            'point_max'=>'required|numeric',
        ]);
        $data = $request->all();
        Sanksi::create($data);
        return redirect()->route('sanksi-admin.index')->with('success','Sanksi Berhasil dibuat');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sanksi=Sanksi::find($id);
        return view('page.admin.sanksi.edit', compact('sanksi'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'kode_sanksi' => 'required|string',
            'sanksi' => 'required|string',
            'point_min'=>'required|numeric',
            'point_max'=>'required|numeric',
        ]);
        $data = $request->all();
        $sanksi= Sanksi::find($id);
        $sanksi->update($data);
        return redirect()->route('sanksi-admin.index')->with('success','Sanksi Berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sanksi = Sanksi::find($id);
        $sanksi->delete();
        return redirect()->route('sanksi-admin.index')->with('success','Sanksi Berhasil dihapus');
    }
}
