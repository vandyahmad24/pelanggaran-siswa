<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\MasterPelanggaran;
use App\Models\PelanggaranSiswa;
use App\Models\User;
use Illuminate\Http\Request;
use PDF;


class LaporanController extends Controller
{
    public function index()
    {
        $user = User::where('level','siswa')->get();
        return view('page.admin.laporan.index', compact('user'));
    }
    public function cetak()
    {
        $user = User::where('level','siswa')->get();
 
        $pdf = PDF::loadview('page.admin.pdf.cetak_all_user',['user'=>$user]);
        return $pdf->stream();
    }
    public function cetakPelanggaranSiswa($id)
    {
        $pelanggaran = PelanggaranSiswa::with('user','MasterPelanggaran')
                        ->where('user_id',$id)->get();
                        
        $user = User::find($id);
 
        $pdf = PDF::loadview('page.admin.pdf.cetak_pelanggaran_siswa',[
            'user'=>$user, 
            'pelanggaran'=>$pelanggaran
        ]);
        return $pdf->stream();
    }
}
