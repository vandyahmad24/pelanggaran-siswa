<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\MasterPelanggaran;
use Illuminate\Http\Request;

class PelanggaranController extends Controller
{
    public function index()
    {
        $master = MasterPelanggaran::orderBy('created_at','desc')->get();
        return view("page.admin.pelanggaran.pelanggaran", compact('master'));
    }
    public function add()
    {
        return view("page.admin.pelanggaran.addpelanggaran");
    }
    public function post(Request $request)
    {
        $validated = $request->validate([
            'kode_pelanggaran' => 'required|unique:master_pelanggaran|max:255',
            'bentuk_pelanggaran' => 'required|string',
            'point' => 'required|numeric'
        ]);
        $data = $request->all();
        MasterPelanggaran::create($data);
        return redirect()->route('pelanggaran-admin')->with('success','Pelanggaran Berhasil dibuat');
    }
    public function delete($id)
    {
        $master = MasterPelanggaran::find($id);
        $master->delete();
        return redirect()->route('pelanggaran-admin')->with('delete','Pelanggaran Berhasil dihapus');
    }
    public function edit($id)
    {
        $master = MasterPelanggaran::find($id);
        return view("page.admin.pelanggaran.editpelanggaran", compact('master'));
    }
    public function put(Request $request,$id)
    {
        $validated = $request->validate([
            'kode_pelanggaran' => 'required',
            'bentuk_pelanggaran' => 'required|string',
            'point' => 'required|numeric'
        ]);
        // cek jika request kode_pelanggaran sudah ada di data lain
        $cek = MasterPelanggaran::where('kode_pelanggaran',$request->kode_pelanggaran)->first();
        if(isset($cek)){
            if($cek->id != $id){
                return redirect()->route('edit-pelanggaran-admin',$id)->with('error','Kode Pelanggaran sudah ada');
            }
        }
        

        $data = $request->all();
        $master = MasterPelanggaran::find($id);
        $master->update($data);
        return redirect()->route('pelanggaran-admin')->with('success','Pelanggaran Berhasil diupdate');
    }
}
