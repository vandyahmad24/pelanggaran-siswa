<?php

namespace App\Http\Controllers;

use App\Models\MasterPelanggaran;
use App\Models\PelanggaranSiswa;
use App\Models\Profile;
use App\Models\User;
use Bantuan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class SiswaController extends Controller
{
    public function index()
    {
        $pelanggaran = Profile::orderBy('jumlah_point','desc')->limit(10)->get();
        $pelanggaranuser = PelanggaranSiswa::where('user_id',Auth::user()->id)->orderBy('waktu_pelanggaran','desc')->get();
        $admin = User::where('level','!=','siswa')->get();
        // dd($admin);
        return view('page.siswa.index', compact('pelanggaran','pelanggaranuser','admin'));
    }
    public function sendMessage(Request $request)
    {
        
        $pesan = str_replace(" ","%20",$request->message);
        $admin = User::find($request->admin);
        // dd($admin);
        if(!isset($admin->no_hp)){
            return redirect()->route('dashboard-siswa')->with('error',"Nomer HP $admin->name tidak valid mohon pilih admin lain");
        }
        $nohp = Bantuan::changeNoHP($admin->no_hp);
        $url = "https://api.whatsapp.com/send?phone=$nohp&text=$pesan&source=&data=";
        return Redirect::to($url);


        // dd($test);
        // 
    }
}
