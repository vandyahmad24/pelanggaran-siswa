<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterPelanggaran extends Model
{
    use HasFactory;
    protected $table = 'master_pelanggaran';
    protected $fillable = [
        'kode_pelanggaran',
        'bentuk_pelanggaran',
        'point',
    ];
    public function PelanggaranSiswa()
    {
        return $this->hasMany(PelanggaranSiswa::class,'id','pelanggaran_id');
    }
    
}
