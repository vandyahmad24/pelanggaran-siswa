<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PelanggaranSiswa extends Model
{
    use HasFactory;
    protected $table = 'pelanggaran_siswa';
    protected $fillable = [
        'waktu_pelanggaran',
        'pelanggaran_id',
        'user_id',
    ];
    public function MasterPelanggaran()
    {
        return $this->belongsTo(MasterPelanggaran::class, 'pelanggaran_id', 'id');
    }
    public function user()
    {
        return $this->belongsTo(User::class,'id');
    }
}
